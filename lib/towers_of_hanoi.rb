# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  attr_reader :towers
  def initialize()
    @towers = [[3,2,1],[],[]]
  end

  def move(from_tower,to_tower)
    @towers[to_tower] << @towers[from_tower].pop
  end

  def valid_move?(from_tower, to_tower)
     !@towers[from_tower].empty? && (
      @towers[to_tower].empty? ||
      @towers[to_tower].last > @towers[from_tower].last
    )
  end

  def won?
    @towers[0].empty? && (@towers[1].empty? || @towers[2].empty?)
  end

  def render
    'Tower 0:  ' + @towers[0].join('  ') + "\n" +
    'Tower 1:  ' + @towers[1].join('  ') + "\n" +
    'Tower 2:  ' + @towers[2].join('  ') + "\n"
  end

  def play
    puts "Welcome to a new Towers of Hanoi game!\n"
    puts "You have three piles and you have to pick a pile and move whatever
          is on top of the pile into another.\n"
    puts "Your goal is to move this tower in full to another pile, moving only one
          disc at a time!\n"
    while true do
      puts "Please enter the index of the pile to select from (0-2)"
      while ! valid_input?(source = gets.chomp.to_i) do
        puts "Please enter A VALID index of the pile to select from (between 0-2)"
      end
      puts "And then the index of the target pile (0-2)"
      while ! valid_input?(target = gets.chomp.to_i) do
        puts "Please enter A VALID index of the target pile between 0-2"
      end

      move(source, target) if valid_move?(source,target)
      display


      if won?
        puts "You have won! The game is finished."
        break
      end
    end

  end

  private

  def valid_input?(input)
    input >= 0 && input <= 2
  end

  def display
    system('clear')
    puts "Updated!"
    puts render
  end
end

# makes the game automatically start the game if this file is run in command line
if $PROGRAM_NAME == __FILE__
  TowersOfHanoi.new().play
end
